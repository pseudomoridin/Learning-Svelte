/** @type {import('./$types').PageLoad} */
export async function load() {
	const posts = import.meta.globEager('../../posts/*.md')
	const postsList = Object.values(posts)
	const postsMeta = postsList.map(post => {
		return post.metadata
	})
	return {
		posts,
		postsList,
		postsMeta,
	}
}
