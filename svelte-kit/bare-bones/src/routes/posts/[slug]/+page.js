/** @type {import('./$types').PageLoad} */
import { error, redirect } from '@sveltejs/kit'

export async function load({ params }) {
	//console.log(params)
	const post = {
		title : params.slug,
		date : new Date(),
		body : "lorem ipsum",
	};
	try {
		const Xerxes = await import(`../../../posts/${params.slug}.md`)
		return {
			post,
			Xerxes: Xerxes.default
		}
	}
	catch (e) {
		console.log('Caught', e)
		throw error(404, 'test')
		throw redirect(303, '/posts')
	}
}